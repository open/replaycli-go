package datasync

import (
	"go.digitalcircle.com.br/open/replaycli-go/api"
)

type Cli struct {
	*api.ApiCli
}

func (c *Cli) New(i interface{}) (string, error) {
	res := ""
	err := c.HttpCli().JsonPost("/ipc/datasyncreportmgr/new", i, &res)
	return res, err
}

func NewCli() *Cli {
	ret := &Cli{ApiCli: api.NewApiCli()}
	return ret
}
