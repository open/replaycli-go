package ocr_test

import (
	"go.digitalcircle.com.br/open/replaycli-go/api/ocr"
	"log"
	"testing"
)

func TestCli_OCR(t *testing.T) {
	c := ocr.NewCli()
	a, err := c.OCR(&ocr.Opts{
		X:        0,
		Y:        0,
		H:        400,
		W:        400,
		Resizew:  1200,
		Sharpen:  1,
		Tempfile: "some_test.jpg",
		Src:      "ss",
		AddRects: true,
		Gray:     true,
	})
	if err != nil {
		t.Fatal(err.Error())
	}
	log.Printf("#%v", a)
}
