package ocr

import (
	"encoding/xml"
	"go.digitalcircle.com.br/open/replaycli-go/api"
	"log"
)

const ScreenShot = "ss"

type AltoString struct {
	Text    string  `xml:",chardata"`
	ID      string  `xml:"ID,attr"`
	HPOS    int     `xml:"HPOS,attr"`
	VPOS    int     `xml:"VPOS,attr"`
	WIDTH   int     `xml:"WIDTH,attr"`
	HEIGHT  int     `xml:"HEIGHT,attr"`
	WC      float64 `xml:"WC,attr"`
	CONTENT string  `xml:"CONTENT,attr"`
	AHPOS   int     `xml:"AHPOS,attr"`
	AVPOS   int     `xml:"AVPOS,attr"`
	AWIDTH  int     `xml:"AWIDTH,attr"`
	AHEIGHT int     `xml:"AHEIGHT,attr"`
}

func (a *AltoString) AbsCoords(q *Opts) {
	nx := int(float64(a.HPOS)/q.Proportion) + q.X
	ny := int(float64(a.VPOS)/q.Proportion) + q.Y
	nw := int(float64(a.WIDTH) / q.Proportion)
	nh := int(float64(a.HEIGHT) / q.Proportion)
	log.Printf("Mapping: x: %v=>%v; y:%v=>%v; w:%v=>%v; h:%v=>%v;(Prop: %v,qx: %v, qy: %v)", a.HPOS, nx, a.VPOS, ny, a.WIDTH, nw, a.HEIGHT, nh, q.Proportion, q.X, q.Y)
	a.AHPOS = nx
	a.AVPOS = ny
	a.AWIDTH = nw
	a.AHEIGHT = nh
	return
}

type AltoSP struct {
	Text  string `xml:",chardata"`
	WIDTH int    `xml:"WIDTH,attr"`
	VPOS  int    `xml:"VPOS,attr"`
	HPOS  int    `xml:"HPOS,attr"`
}
type AltoTextLine struct {
	Text   string        `xml:",chardata"`
	ID     string        `xml:"ID,attr"`
	HPOS   string        `xml:"HPOS,attr"`
	VPOS   string        `xml:"VPOS,attr"`
	WIDTH  string        `xml:"WIDTH,attr"`
	HEIGHT string        `xml:"HEIGHT,attr"`
	String []*AltoString `xml:"String"`
	SP     []*AltoSP     `xml:"SP"`
}
type AltoTextBlock struct {
	Text     string          `xml:",chardata"`
	ID       string          `xml:"ID,attr"`
	HPOS     int             `xml:"HPOS,attr"`
	VPOS     int             `xml:"VPOS,attr"`
	WIDTH    int             `xml:"WIDTH,attr"`
	HEIGHT   int             `xml:"HEIGHT,attr"`
	TextLine []*AltoTextLine `xml:"TextLine"`
}
type AltoComposedBlock struct {
	Text      string           `xml:",chardata"`
	ID        string           `xml:"ID,attr"`
	HPOS      int              `xml:"HPOS,attr"`
	VPOS      int              `xml:"VPOS,attr"`
	WIDTH     int              `xml:"WIDTH,attr"`
	HEIGHT    int              `xml:"HEIGHT,attr"`
	TextBlock []*AltoTextBlock `xml:"TextBlock"`
}
type AltoPrintSpace struct {
	Text          string               `xml:",chardata"`
	HPOS          int                  `xml:"HPOS,attr"`
	VPOS          int                  `xml:"VPOS,attr"`
	WIDTH         int                  `xml:"WIDTH,attr"`
	HEIGHT        int                  `xml:"HEIGHT,attr"`
	ComposedBlock []*AltoComposedBlock `xml:"ComposedBlock"`
}
type AltoPage struct {
	Text          string          `xml:",chardata"`
	WIDTH         int             `xml:"WIDTH,attr"`
	HEIGHT        int             `xml:"HEIGHT,attr"`
	PHYSICALIMGNR string          `xml:"PHYSICAL_IMG_NR,attr"`
	ID            string          `xml:"ID,attr"`
	PrintSpace    *AltoPrintSpace `xml:"PrintSpace"`
}
type AltoDescription struct {
	Text                   string `xml:",chardata"`
	MeasurementUnit        string `xml:"MeasurementUnit"`
	SourceImageInformation struct {
		Text     string `xml:",chardata"`
		FileName string `xml:"fileName"`
	} `xml:"sourceImageInformation"`
	OCRProcessing *struct {
		Text              string `xml:",chardata"`
		ID                string `xml:"ID,attr"`
		OcrProcessingStep struct {
			Text               string `xml:",chardata"`
			ProcessingSoftware struct {
				Text         string `xml:",chardata"`
				SoftwareName string `xml:"softwareName"`
			} `xml:"processingSoftware"`
		} `xml:"ocrProcessingStep"`
	} `xml:"OCRProcessing"`
}
type AltoLayout struct {
	Text string    `xml:",chardata"`
	Page *AltoPage `xml:"Page"`
}
type Alto struct {
	XMLName        xml.Name         `xml:"alto"`
	Text           string           `xml:",chardata"`
	Xmlns          string           `xml:"xmlns,attr"`
	Xlink          string           `xml:"xlink,attr"`
	Xsi            string           `xml:"xsi,attr"`
	SchemaLocation string           `xml:"schemaLocation,attr"`
	Description    *AltoDescription `xml:"Description"`
	Layout         *AltoLayout      `xml:"Layout"`
}

func (a *Alto) Strings() []*AltoString {
	ret := make([]*AltoString, 0)
	for _, v := range a.Layout.Page.PrintSpace.ComposedBlock {
		for _, v1 := range v.TextBlock {
			for _, v2 := range v1.TextLine {
				for _, v3 := range v2.String {
					ret = append(ret, v3)
				}
			}
		}
	}
	return ret
}
func (a *Alto) CalcAbsolute(q *Opts) {
	for _, v := range a.Layout.Page.PrintSpace.ComposedBlock {
		for _, v1 := range v.TextBlock {
			for _, v2 := range v1.TextLine {
				for _, v3 := range v2.String {
					v3.AbsCoords(q)
				}
			}
		}
	}

}

type Opts struct {
	Ver        string  `json:"ver,omitempty"`
	X          int     `json:"x"`
	Y          int     `json:"y"`
	H          int     `json:"h"`
	W          int     `json:"w"`
	Blur       float64 `json:"blur"`
	Sharpen    float64 `json:"sharpen"`
	Resizew    int     `json:"resizew"`
	Resizeh    int     `json:"resizeh"`
	Lang       string  `json:"lang"`
	Tempfile   string  `json:"tempfile"`
	Dispid     int     `json:"d"`
	Gray       bool    `json:"gray"`
	Src        string  `json:"src"`
	Bytes      []byte  `json:"bytes"`
	AddRects   bool    `json:"add_rects"`
	Proportion float64 `json:"proportion"`
	Encoding   string  `json:"encoding"`
	Absolute   bool    `json:"absolute"`
}

type Cli struct {
	*api.ApiCli
}

func (c *Cli) OCR(opts *Opts) (*Alto, error) {
	res := &Alto{}
	err := c.HttpCli().JsonPost("/ipc/ocr/", opts, res)
	return res, err
}

func NewCli() *Cli {
	ret := &Cli{ApiCli: api.NewApiCli()}
	return ret
}
