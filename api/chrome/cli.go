package chrome

import (
	"encoding/json"
	"fmt"

	"go.digitalcircle.com.br/open/replaycli-go/api"
)

type Cli struct {
	*api.ApiCli
}

type TabMetadata struct {
	Description          string `json:"description"`
	DevtoolsFrontendUrl  string `json:"devtoolsFrontendUrl"`
	Id                   string `json:"id"`
	Title                string `json:"title"`
	Type                 string `json:"type"`
	Url                  string `json:"url"`
	WebSocketDebuggerUrl string `json:"webSocketDebuggerUrl"`
}

func (c *Cli) Start(to int) error {
	// to -> Time out para a conexão com o Chrome. Dado em segundos.
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/chrome/start?to=%d", to), nil)
	return err
}
func (c *Cli) StartHeadless() error {
	err := c.HttpCli().JsonGet("/ipc/chrome/startHeadless", nil)
	return err
}
func (c *Cli) Stop() error {
	err := c.HttpCli().JsonGet("/ipc/chrome/stop", nil)
	return err
}
func (c *Cli) New(url string) (string, error) {
	// url -> Site no qual se quer iniciar uma nova aba.
	ret := ""
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/chrome/new?url=%s", url), &ret)
	return ret, err
	// ret -> ID do Websocket criado para navegação na "url" informada.
}
func (c *Cli) Close(id string) error {
	// id -> ID do websocket ativo. É o primeiro retorno do método "New".
	err := c.HttpCli().JsonGet("/ipc/chrome/close/"+id, nil)
	return err
}
func (c *Cli) Eval(id string, s string) (map[string]interface{}, error) {
	// id -> ID do websocket no qual se quer fazer um comando JavaScript.
	// s -> Comando JavaScript a ser executado no terminal web do WebSocket informado.
	ret := make(map[string]interface{})
	bs, err := c.HttpCli().RawPost("/ipc/chrome/eval/"+id, []byte(s))
	json.Unmarshal(bs, &ret)
	return ret, err
	// ret -> O valor retornado pelo comando JavaScript executado.
}
func (c *Cli) Wait(id string, s string, to int) (string, error) {
	// id -> ID do websocket no qual se quer fazer um comando JavaScript.
	// s -> Comando JavaScript a ser executado no terminal web do WebSocket informado. Deve ser uma expressão que retorne um valor booleano para, por exemplo, verificar se um elemento já foi carregado.
	// to -> TimeOut de espera para retorno "true" da expressão JavaScript informada. Informado em segundos.
	ret := ""
	bs, err := c.HttpCli().RawPost(fmt.Sprintf("/ipc/chrome/wait/%s?to=%d", id, to), []byte(s))
	json.Unmarshal(bs, &ret)
	return ret, err
}
func (c *Cli) Send(id string, m string, ps map[string]interface{}) (interface{}, error) {
	// id -> ID do Websocket no qual se deseja realizar a alteração.
	var ret interface{}
	in := map[string]interface{}{
		"method": m,
		"params": ps,
	}
	err := c.HttpCli().JsonPost("/ipc/chrome/send/"+id, in, &ret)
	return ret, err
	// ret -> Retorno do comando (Referência em: https://chromedevtools.github.io/devtools-protocol/)
}

func (c *Cli) OpenTabs() (ret []map[string]string, err error) {
	err = c.HttpCli().JsonGet("/ipc/chrome/opentabs", &ret)
	return ret, err
	// ret -> Array contendo as abas abertas e metadados sobre as mesmas.
}

func (c *Cli) FindTabByUrl(url string) (ret string, err error) {
	// url -> URL ativa em uma das abas abertas.
	err = c.HttpCli().JsonGet("/ipc/chrome/findtabbyurl/"+url, &ret)
	return ret, err
	// ret -> ID do WebSocket contendo a aba encontrada.
}

func (c *Cli) FindUrlById(id string) (ret string, err error) {
	// url -> URL ativa em uma das abas abertas.
	err = c.HttpCli().JsonGet("/ipc/chrome/findurlbyid/"+id, &ret)
	return ret, err
	// ret -> Url da aba encontrada.
}

func (c *Cli) FindTabByTitle(title string) (ret string, err error) {
	// title -> Título de uma das abas abertas.
	err = c.HttpCli().JsonGet("/ipc/chrome/findtabbytitle/"+title, &ret)
	return ret, err
	// ret -> ID do WebSocket contendo a aba encontrada.
}

func NewCli() *Cli {
	ret := &Cli{ApiCli: api.NewApiCli()}
	return ret
	// ret -> Nova instância do Chrome para utilização.
}
