package autohotkey

import (
	"go.digitalcircle.com.br/open/replaycli-go/api"
)

type Cli struct {
	*api.ApiCli
}

func NewCli() *Cli {
	ret := &Cli{ApiCli: api.NewApiCli()}
	return ret
}

func (c *Cli) Test() (ret []byte, err error) {
	ret, err = c.HttpCli().RawPost("/ipc/ahk/test", nil)
	return ret, err
}

func (c *Cli) Do(data string) (ret []byte, err error) {
	i := []byte(data)
	ret, err = c.HttpCli().RawPost("/ipc/ahk/do", i)
	return ret, err
}
