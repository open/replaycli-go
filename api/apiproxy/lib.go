package apiproxy

import "go.digitalcircle.com.br/open/replaycli-go/api"

type APIProxyRequest struct {
	Name     string            `json:"name,omitempty"`
	Method   string            `json:"method,omitempty"`
	Url      string            `json:"url,omitempty"`
	Header   map[string]string `json:"header,omitempty"`
	Body     []byte            `json:"body,omitempty"`
	Readonly bool              `json:"readonly"`
}

type APIProxyResponse struct {
	Status     string            `json:"status"`
	StatusCode int               `json:"status_code"`
	Header     map[string]string `json:"header,omitempty"`
	Body       []byte            `json:"body,omitempty"`
}

type Cli struct {
	*api.ApiCli
}

func (c *Cli) Do(req *APIProxyRequest) (*APIProxyResponse, error) {
	res := &APIProxyResponse{}
	err := c.HttpCli().JsonPost("/ipc/apiproxy/do", req, res)
	return res, err
}

func NewCli() *Cli {
	ret := &Cli{ApiCli: api.NewApiCli()}
	return ret
}
