package apiproxy

import (
	"log"
	"os"
	"testing"
)

func TestCli_Do(t *testing.T) {
	os.Setenv("REPLAY_APIKEY", "DONTBOTHER")
	os.Setenv("REPLAY_ADDR", "https://localhost:8443")
	c := NewCli()

	res, err := c.Do(&APIProxyRequest{
		Method: "GET",
		Url:    "https://www.slashdot.org",
	})
	if err != nil {
		t.Fatal(err.Error())
	}
	log.Printf("%#v", res)
}
