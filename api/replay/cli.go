package replay

import (
	"encoding/json"
	"fmt"
	"os"
	"runtime"
	"time"

	"go.digitalcircle.com.br/open/replaycli-go/api"
	"go.digitalcircle.com.br/open/replaycli-go/types"
	"go.digitalcircle.com.br/open/replaycli-go/util"
)

type Cli struct {
	*api.ApiCli
}

//Log saves log to database after populated
func (c *Cli) Log(l *types.Log) error {
	l.Alias = c.ReplayEnvAlias()
	l.Dtlogend = time.Now()
	return c.HttpCli().JsonPost("/api/v1/log/add", l, nil)
}

//NewLog creates a new log instance in the client, capable of collecting data in memory for later saving it to the
//database by calling Log
func (c *Cli) NewLog() *types.Log {
	ret := &types.Log{}
	ret.Alias = c.ReplayEnvAlias()
	ret.Dtlog = time.Now()
	return ret
}

func (c *Cli) ReplayEnvQueueId() string {
	return os.Getenv("REPLAY_QUEUEID")
}
func (c *Cli) ReplayEnvAlias() string {
	return os.Getenv("REPLAY_ALIAS")
}
func (c *Cli) ReplayEnvFeatureDir() string {
	return os.Getenv("REPLAY_FEATUREDIR")
}
func (c *Cli) ReplayEnvRoot() string {
	return os.Getenv("REPLAY_ROOT")
}
func (c *Cli) ReplayEnvAddr() string {
	return os.Getenv("REPLAY_ADDR")
}
func (c *Cli) ReplayEnvHomeDir() string {
	return os.Getenv("REPLAY_HOMEDIR")
}

func (c *Cli) ReplayPort() string {
	return os.Getenv("REPLAY_PORT")
}

func (c *Cli) ReplayEnvRepo() string {
	return os.Getenv("REPLAY_REPO")
}
func (c *Cli) ReplayEnvVer() string {
	return os.Getenv("REPLAY_VER")
}
func (c *Cli) ReplayEnvRepoDir() string {
	return os.Getenv("REPLAY_REPODIR")
}
func (c *Cli) ReplayEnvDataDir() string {
	return os.Getenv("REPLAY_DATADIR")
}
func (c *Cli) ReplayEnvInstanceAlias() string {
	return os.Getenv("REPLAY_INSTANCE_ALIAS")
}
func (c *Cli) ReplayEnvApiKey() string {
	return os.Getenv("REPLAY_APIKEY")
}

func (c *Cli) OpenApp(id uint) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/api/v1/app/run/%d", id), nil)
	return err
}

func (c *Cli) ConfigGet(k string) (ret string, err error) {
	err = c.HttpCli().JsonGet("/api/v1/config/byrepo/"+util.Repo()+"/"+k, &ret)
	return
}
func (c *Cli) ConfigGetGlobal(k string) (ret string, err error) {
	err = c.HttpCli().JsonGet("/api/v1/config/global/"+k, &ret)
	return
}
func (c *Cli) ConfigGetAll() (ret []types.Config, err error) {
	ret = make([]types.Config, 0)
	err = c.HttpCli().JsonGet("/api/v1/config/byrepo/"+util.Repo(), &ret)
	return
}
func (c *Cli) ConfigGetMap() (ret map[string]types.Config, err error) {
	cfgs, err := c.ConfigGetAll()
	if err != nil {
		return
	}
	ret = make(map[string]types.Config)
	for _, v := range cfgs {
		ret[v.K] = v
	}
	return
}

func (c *Cli) CronReload() error {
	return c.HttpCli().JsonGet("/ipc/cron/reload", nil)
}

func (c *Cli) Exit() error {
	err := c.HttpCli().JsonGet("/api/v1/exit", nil)
	return err
}

func (c *Cli) MenuGetAllEnabled() (ret []types.Menu, err error) {
	ret = make([]types.Menu, 0)
	err = c.HttpCli().JsonGet("/api/v1/menu/enabled", &ret)
	return ret, err
}

func (c *Cli) QueueAdd(job string, bs []byte) error {
	_, err := c.HttpCli().RawPost("/api/v1/queue/add/"+job, bs)
	return err
}
func (c *Cli) QueueGetData(id string) (ret []byte, err error) {
	ret, err = c.HttpCli().RawGet("/api/v1/queue/getrequest/" + id)
	return
}

func (c *Cli) QueueGetDataBody(id string) (ret []byte, err error) {
	ret, err = c.HttpCli().RawGet("/api/v1/queue/getrequestbody/" + id)
	return
}
func (c *Cli) QueueGetMyData() ([]byte, error) {
	return c.QueueGetData(os.Getenv("REPLAY_QUEUEID"))
}
func (c *Cli) QueueGetMyJson(i *interface{}) error {
	bs, err := c.QueueGetData(os.Getenv("REPLAY_QUEUEID"))
	if err != nil {
		return err
	}
	err = json.Unmarshal(bs, &i)
	return err
}
func (c *Cli) QueueAbort() error {
	return c.HttpCli().JsonGet("/api/v1/queue/abort", nil)
}
func (c *Cli) QueueEnqueue(id uint) error {
	return c.HttpCli().JsonGet(fmt.Sprintf("/api/v1/robots/op/enqueue/%d", id), nil)
}

func (c *Cli) QueueEnqueueWData(id string, bs []byte) ([]byte, error) {
	return c.HttpCli().RawPost(fmt.Sprintf("api/v1/queue/add/%s", id), bs)
}

func (c *Cli) QueueEnqueueWDataWait(id string, bs []byte) ([]byte, error) {
	return c.HttpCli().RawPost(fmt.Sprintf("api/v1/queue/addwait?job=%s", id), bs)
}

func (c *Cli) ServiceStopAll() error {
	return c.HttpCli().JsonGet("/api/v1/service/op/stopall", nil)
}

func (c *Cli) Err(err string, desc string) error {
	bs := make([]byte, 4096)
	n := runtime.Stack(bs, false)
	cd, _ := c.QueueGetMyData()
	e := &types.Error{
		Feature:   c.ReplayEnvAlias(),
		Err:       err,
		When:      time.Time{},
		Stack:     string(bs[:n]),
		InputData: string(cd),
		Details:   desc,
	}
	return c.HttpCli().JsonPost("/api/v1/err", e, nil)
}

type SQLReturn struct {
	Data []map[string]interface{} `json:"data"`
	Err  string                   `json:"err"`
}

func (c *Cli) SQL(s string) (*SQLReturn, error) {
	in := &struct {
		Sql string `json:"sql"`
	}{s}
	out := &SQLReturn{}
	err := c.HttpCli().JsonPost("/api/v1/sql", in, &out)
	return out, err
}

func NewCli() *Cli {
	ret := &Cli{ApiCli: api.NewApiCli()}
	return ret
}
