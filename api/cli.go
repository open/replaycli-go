package api

import (
	"go.digitalcircle.com.br/open/httpcli"
	"go.digitalcircle.com.br/open/replaycli-go/ipcmux"
	"go.digitalcircle.com.br/open/replaycli-go/util"
	"os"
)

type ApiCli struct {
	cli *httpcli.Cli
}

func (a *ApiCli) HttpCli() *httpcli.Cli {
	return a.cli
}

func NewApiCli() *ApiCli {
	ret := &ApiCli{cli: httpcli.C()}
	ret.cli.SetBasePath(util.Addr())
	apikey = os.Getenv("REPLAY_APIKEY")
	ret.cli.AddHeader("X-API-KEY", apikey)
	return ret
}

func NewApiIPCCli() *ApiCli {
	ret := &ApiCli{cli: httpcli.NewCli()}
	ret.cli.SetCli(ipcmux.NewClient())
	apikey = os.Getenv("REPLAY_APIKEY")
	ret.cli.AddHeader("X-API-KEY", apikey)
	return ret
}
