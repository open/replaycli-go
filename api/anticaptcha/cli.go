package anticaptcha

import (
	"go.digitalcircle.com.br/open/replaycli-go/api"
)

type Cli struct {
	*api.ApiCli
	//cli *api.Cli
}

//func (c *Cli) HttpCli() *api.Cli {
//
//	return c.HttpCli()
//}

type Req struct {
	Site string `json:"site"`
	Data string `json:"data"`
	Img  string `json:"img"`
	To   int    `json:"to"`
}

func (c *Cli) Recaptchav2(site string, data string) (string, error) {
	ret := ""
	req := &Req{
		Site: site,
		Data: data,
		Img:  "",
		To:   300,
	}
	err := c.HttpCli().JsonPost("/ipc/anticaptcha/recaptchav2", req, &ret)
	return ret, err
}

func (c *Cli) Hcaptcha(site string, data string) (string, error) {
	ret := ""
	req := &Req{
		Site: site,
		Data: data,
		Img:  "",
		To:   300,
	}
	err := c.HttpCli().JsonPost("/ipc/anticaptcha/hcaptcha", req, &ret)
	return ret, err
}

func (c *Cli) Image2text(site string, data string) (string, error) {
	ret := ""
	req := &Req{
		Site: site,
		Img:  data,
		To:   300,
	}
	err := c.HttpCli().JsonPost("/ipc/anticaptcha/image2text", req, &ret)
	return ret, err
}

func NewCli() *Cli {
	ret := &Cli{ApiCli: api.NewApiCli()}
	return ret
}
