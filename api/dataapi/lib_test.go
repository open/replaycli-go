package dataapi_test

import (
	"log"
	"strconv"
	"testing"
	"time"

	"go.digitalcircle.com.br/open/replaycli-go/api/dataapi"
)

func TestAPIRetrieve(t *testing.T) {
	c := dataapi.NewCli()
	res, err := c.Do(&dataapi.DataAPIRequest{
		Col: "teste3",
		Op:  dataapi.RETRIEVE,
	})
	if err != nil {
		t.Fatal(err.Error())
	}

	log.Printf("%#v", res)
}

func TestAPIRetrieveWQuery(t *testing.T) {
	c := dataapi.NewCli()
	res, err := c.Do(&dataapi.DataAPIRequest{
		Col: "C1",
		Op:  dataapi.RETRIEVE,
		Q:   "@[?ID==`2`]",
	})
	if err != nil {
		t.Fatal(err.Error())
	}

	log.Printf("%#v", res)
}

func TestAPIUpdate(t *testing.T) {
	c := dataapi.NewCli()
	res, err := c.Do(&dataapi.DataAPIRequest{
		Col: "C1",
		Op:  dataapi.UPDATE,
		Id:  "2",
		Data: map[string]interface{}{
			"F2": "Novo Campo",
		},
	})
	if err != nil {
		t.Fatal(err.Error())
	}

	log.Printf("%#v", res)
}

func TestAPIDelete(t *testing.T) {
	c := dataapi.NewCli()
	res, err := c.Do(&dataapi.DataAPIRequest{
		Col: "C1",
		Op:  dataapi.DELETE,
		Id:  "2",
	})
	if err != nil {
		t.Fatal(err.Error())
	}

	log.Printf("%#v", res)
}

func TestAPICreate(t *testing.T) {
	if true {
		print("a")
		print("b")
	}
	c := dataapi.NewCli()
	var registros int
	var erro int
	current := time.Now().Format("2006-01")
	res, err := c.Do(&dataapi.DataAPIRequest{
		Col: "teste3",
		Op:  dataapi.RETRIEVE,
		Q:   "@[?date=='" + current + "']",
	})
	if err != nil {
		panic(err)
	}
	check := false
	if len(res.Data) > 0 {
		if check {
			result := res.Data[0].(map[string]interface{})
			registros = int(result["exec"].(float64))
			registros = registros + 1
			ident := strconv.Itoa(int(result["ID"].(float64)))

			_, err = c.Do(&dataapi.DataAPIRequest{
				Col: "teste3",
				Op:  dataapi.UPDATE,
				Id:  ident,
				Data: map[string]interface{}{
					"exec": registros,
				},
			})
			if err != nil {
				panic(err)
			}
		} else {
			result := res.Data[0].(map[string]interface{})
			registros = int(result["err"].(float64))
			registros = registros + 1
			ident := strconv.Itoa(int(result["ID"].(float64)))

			_, err = c.Do(&dataapi.DataAPIRequest{
				Col: "teste3",
				Op:  dataapi.UPDATE,
				Id:  ident,
				Data: map[string]interface{}{
					"err": registros,
				},
			})
			if err != nil {
				panic(err)
			}
		}
	} else {
		if check {
			registros = 1
			erro = 0
		} else {
			registros = 0
			erro = 1
		}
		_, err = c.Do(&dataapi.DataAPIRequest{
			Col: "teste3",
			Op:  dataapi.CREATE,
			Data: map[string]interface{}{
				"date": current,
				"exec": registros,
				"err":  erro,
			},
		})
		if err != nil {
			panic(err)
		}
	}
}
