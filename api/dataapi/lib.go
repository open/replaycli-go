package dataapi

import (
	"strconv"
	"time"

	"go.digitalcircle.com.br/open/httpcli"
)

type DataApiOp string

const (
	RETRIEVE DataApiOp = "R"
	CREATE   DataApiOp = "C"
	DELETE   DataApiOp = "D"
	UPDATE   DataApiOp = "U"
)

type DataAPIRequest struct {
	Col  string                 `json:"col"`
	Q    string                 `json:"q"`
	Op   DataApiOp              `json:"op"`
	Id   string                 `json:"id"`
	Data map[string]interface{} `json:"data"`
}

type DataAPIResponse struct {
	Msg  string        `json:"msg"`
	Id   string        `json:"id"`
	Data []interface{} `json:"data"`
}

type Cli struct {
	Httpcli *httpcli.Cli
}

func (c *Cli) Do(request *DataAPIRequest) (response *DataAPIResponse, err error) {
	response = &DataAPIResponse{}
	err = c.Httpcli.JsonPost("/", request, response)
	return
}

func NewCli(apikey string) *Cli {
	ret := &Cli{Httpcli: httpcli.NewCli()}
	ret.Httpcli.AddHeader("X-API-KEY", apikey)
	ret.Httpcli.SetBasePath("https://dataapi.digitalcircle.com.br")
	return ret
}

func RegistrarExec(table, apikey string, check bool, checkC bool) {
	//Para robôs, age como contador e registra cada repetição no mês
	c := NewCli(apikey)
	var registros int
	var erro int
	captReg := 0
	current := time.Now().Format("2006-01")
	res, err := c.Do(&DataAPIRequest{
		Col: table,
		Op:  RETRIEVE,
		Q:   "@[?date=='" + current + "']",
	})
	if err != nil {
		panic(err)
	}
	if len(res.Data) > 0 {
		if check {
			result := res.Data[0].(map[string]interface{})
			registros = int(result["exec"].(float64))
			registros = registros + 1
			captReg = int(result["capt"].(float64))
			if checkC {
				captReg = captReg + 1
			}
			ident := strconv.Itoa(int(result["ID"].(float64)))

			_, err = c.Do(&DataAPIRequest{
				Col: table,
				Op:  UPDATE,
				Id:  ident,
				Data: map[string]interface{}{
					"exec": registros,
					"capt": captReg,
				},
			})
			if err != nil {
				panic(err)
			}
		} else {
			result := res.Data[0].(map[string]interface{})
			registros = int(result["err"].(float64))
			registros = registros + 1
			ident := strconv.Itoa(int(result["ID"].(float64)))

			_, err = c.Do(&DataAPIRequest{
				Col: table,
				Op:  UPDATE,
				Id:  ident,
				Data: map[string]interface{}{
					"err": registros,
				},
			})
			if err != nil {
				panic(err)
			}
		}
	} else {
		if check {
			registros = 1
			erro = 0
			if checkC {
				captReg = 1
			}
		} else {
			registros = 0
			erro = 1
		}
		_, err = c.Do(&DataAPIRequest{
			Col: table,
			Op:  CREATE,
			Data: map[string]interface{}{
				"date": current,
				"exec": registros,
				"err":  erro,
				"capt": captReg,
			},
		})
		if err != nil {
			panic(err)
		}
	}
}
