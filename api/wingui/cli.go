package wingui

import (
	"fmt"

	"go.digitalcircle.com.br/open/replaycli-go/api"
)

type Cli struct {
	*api.ApiCli
}

const (
	CCHDEVICENAME                 = 32
	CCHFORMNAME                   = 32
	ENUM_CURRENT_SETTINGS  uint32 = 0xFFFFFFFF
	ENUM_REGISTRY_SETTINGS uint32 = 0xFFFFFFFE
	DISP_CHANGE_SUCCESSFUL uint32 = 0
	DISP_CHANGE_RESTART    uint32 = 1
	DISP_CHANGE_FAILED     uint32 = 0xFFFFFFFF
	DISP_CHANGE_BADMODE    uint32 = 0xFFFFFFFE
)

type DEVMODE struct {
	DmDeviceName       [CCHDEVICENAME]uint16
	DmSpecVersion      uint16
	DmDriverVersion    uint16
	DmSize             uint16
	DmDriverExtra      uint16
	DmFields           uint32
	DmOrientation      int16
	DmPaperSize        int16
	DmPaperLength      int16
	DmPaperWidth       int16
	DmScale            int16
	DmCopies           int16
	DmDefaultSource    int16
	DmPrintQuality     int16
	DmColor            int16
	DmDuplex           int16
	DmYResolution      int16
	DmTTOption         int16
	DmCollate          int16
	DmFormName         [CCHFORMNAME]uint16
	DmLogPixels        uint16
	DmBitsPerPel       uint32
	DmPelsWidth        uint32
	DmPelsHeight       uint32
	DmDisplayFlags     uint32
	DmDisplayFrequency uint32
	DmICMMethod        uint32
	DmICMIntent        uint32
	DmMediaType        uint32
	DmDitherType       uint32
	DmReserved1        uint32
	DmReserved2        uint32
	DmPanningWidth     uint32
	DmPanningHeight    uint32
}

/***************** Clip Functions *****************/

func (c *Cli) ClipRead() (string, error) {
	bs, err := c.HttpCli().RawGet("/ipc/wingui/clip/read")
	return string(bs), err
}

func (c *Cli) ClipWrite(site string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/clip/write?str=%s", site), nil)
	return err
}

/***************** Mouse Functions *****************/

func (c *Cli) MouseClick() error {
	err := c.HttpCli().JsonGet("/ipc/wingui/mouse/click", nil)
	return err
}

func (c *Cli) MouseMove(x, y int) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/mouse/move?x=%d&y=%d", x, y), nil)
	return err
}

func (c *Cli) MouseMoveRelative(x, y int) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/mouse/moverelative?x=%d&y=%d", x, y), nil)
	return err
}

func (c *Cli) MouseClickRelative(x, y int) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/mouse/clickatrelative?x=%d&y=%d", x, y), nil)
	return err
}

func (c *Cli) MouseClickAt(x, y int) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/mouse/clickat?x=%d&y=%d", x, y), nil)
	return err
}

func (c *Cli) MouseDrag(x, y int, btn string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/mouse/drag?x=%d&y=%d&btn=%s", x, y, btn), nil)
	return err
}

/***************** KB Functions *****************/

func (c *Cli) KBTap(t string) (map[string]interface{}, error) {
	/*Envia uma tecla ou teclas para serem clicadas no componente com foco onde o robo é executado
	  Em caso de teclas alteradoras, elas devem ser sempre posicionadas apos a tecla alvo.
	  Dessa forma, para enviar ALT+d, vc envia o string "d,alt". Para enviar C maiusculo, vc utiiliza
	  "c,shift".

	  Referencia das teclas e respectivos strings:

	  "`": 41,
	  "1": 2,
	  "2": 3,
	  "3": 4,
	  "4": 5,
	  "5": 6,
	  "6": 7,
	  "7": 8,
	  "8": 9,
	  "9": 10,
	  "0": 11,
	  "-": 12,
	  "+": 13,
	  //
	  "q":  16,
	  "w":  17,
	  "e":  18,
	  "r":  19,
	  "t":  20,
	  "y":  21,
	  "u":  22,
	  "i":  23,
	  "o":  24,
	  "p":  25,
	  "[":  26,
	  "]":  27,
	  "\\": 43,
	  //
	  "a": 30,
	  "s": 31,
	  "d": 32,
	  "f": 33,
	  "g": 34,
	  "h": 35,
	  "j": 36,
	  "k": 37,
	  "l": 38,
	  ";": 39,
	  "'": 40,
	  //
	  "z": 44,
	  "x": 45,
	  "c": 46,
	  "v": 47,
	  "b": 48,
	  "n": 49,
	  "m": 50,
	  ",": 51,
	  ".": 52,
	  "/": 53,
	  //
	  "f1":  59,
	  "f2":  60,
	  "f3":  61,
	  "f4":  62,
	  "f5":  63,
	  "f6":  64,
	  "f7":  65,
	  "f8":  66,
	  "f9":  67,
	  "f10": 68,
	  "f11": 69,
	  "f12": 70,
	  // more
	  "esc":     1,
	  "delete":  14,
	  "tab":     15,
	  "ctrl":    29,
	  "control": 29,
	  "alt":     56,
	  "space":   57,
	  "shift":   42,
	  "rshift":  54,
	  "enter":   28,
	  //
	  "cmd":     3675,
	  "command": 3675,
	  "rcmd":    3676,
	  "ralt":    3640,
	  //
	  "up":    57416,
	  "down":  57424,
	  "left":  57419,
	  "right": 57421,
	*/

	ret := make(map[string]interface{})
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/kb/tap?str=%s", t), &ret)
	return ret, err
}

func (c *Cli) KBType(t string) (map[string]interface{}, error) {
	/* Semelhante a tap, mas envia textos completos.
	 ***ATENCAO*** Esse metodo usa urlencoding, dessa forma caracteres serao codificados para envio. # virará %23
	 por exemplo. Ao chegar no robo passrão pelo decoding análogo. */

	ret := make(map[string]interface{})
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/kb/type?str=%s", t), &ret)
	return ret, err
}

func (c *Cli) KBToggle(str string) error {
	err := c.HttpCli().JsonGet("/ipc/wingui/kb/toggle?str="+str, nil)
	return err
}

/***************** Display Functions *****************/

func (c *Cli) DisplayRes() (*DEVMODE, error) {
	ret := &DEVMODE{}
	err := c.HttpCli().JsonGet("/ipc/wingui/display/res", ret)
	return ret, err
}

func (c *Cli) DisplaySetRes(d *DEVMODE) error {
	err := c.HttpCli().JsonPost("/ipc/wingui/display/setres", d, nil)
	return err
}

/***************** Draw Functions *****************/

func (c *Cli) DrawCLear() error {
	err := c.HttpCli().JsonGet("/ipc/draw/clear", nil)
	return err
}

func (c *Cli) DrawBox(x, y, w, h, t int) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/draw/addbox?x=%d&y=%d&w=%d&h=%d&t=%d", x, y, w, h, t), nil)
	return err
}

func (c *Cli) DrawRect(x, y, w, h int) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/draw/addrect?x=%d&y=%d&w=%d&h=%d", x, y, w, h), nil)
	return err
}

func (c *Cli) DrawAddText(t string, x, y int) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/draw/addtext?t=%s&x=%d&y=%d", t, x, y), nil)
	return err
}

/***************** Notify Functions *****************/

func (c *Cli) NotifyAlert(msg, title string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/notify/alert?msg=%s&title=%s", msg, title), nil)
	return err
}

func (c *Cli) NotifyBeep(msg, title string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/notify/notify?msg=%s&title=%s", msg, title), nil)
	return err
}

/***************** Window Functions *****************/

func (c *Cli) WindowList(s string) (ret []map[string]interface{}, err error) {
	err = c.HttpCli().JsonGet("/ipc/wingui/window/list?s="+s, &ret)
	return ret, err
}

func (c *Cli) WindowHwnd(s string) (ret []int, err error) {
	err = c.HttpCli().JsonGet("/ipc/wingui/window/hwnd?s="+s, &ret)
	return ret, err
}

func (c *Cli) WindowActivate(Hwnd string) error {
	err := c.HttpCli().JsonGet("/ipc/wingui/window/activate?Hwnd="+Hwnd, nil)
	return err
}

func (c *Cli) WindowClose(Hwnd string) error {
	err := c.HttpCli().JsonGet("/ipc/wingui/window/close?Hwnd="+Hwnd, nil)
	return err
}

func (c *Cli) WindowMax(Hwnd string) error {
	err := c.HttpCli().JsonGet("/ipc/wingui/window/max?Hwnd="+Hwnd, nil)
	return err
}

func (c *Cli) WindowMin(Hwnd string) error {
	err := c.HttpCli().JsonGet("/ipc/wingui/window/min?Hwnd="+Hwnd, nil)
	return err
}

func (c *Cli) WindowActiveHwnd() (ret int, err error) {
	err = c.HttpCli().JsonGet("/ipc/wingui/window/activehwnd", &ret)
	return ret, err
}

func (c *Cli) WindowWaitActiveTitle(t string, m int) (ret map[string]interface{}, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/window/waitactivetitle?t=%s&m=%d", t, m), &ret)
	return ret, err
}

func (c *Cli) WindowActiveTitle() (ret string, err error) {
	err = c.HttpCli().JsonGet("/ipc/wingui/window/activetitle", &ret)
	return ret, err
}

/*func (c *Cli) WindowWinfo(w int) (ret interface{}, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/window/winfo?w=%d", w), &ret)
	return ret, err
}

func (c *Cli) WindowSendMsg(w, m string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/window/sendmsg?w=%s&m=%s", w, m), nil)
	return err
}

func (c *Cli) WindowPostMsg(w, m string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/window/postmsg?w=%s&m=%s", w, m), nil)
	return err
}*/

/***************** Screen Functions *****************/

func (c *Cli) ScreenCapture(x, y, w, h int, f string) (ret interface{}, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/screen/capture?x=%d&y=%d&w=%d&h=%d&f=%s", x, y, w, h, f), &ret)
	return ret, err
}

func (c *Cli) ScreenFindAll(f string) (ret map[string]int, err error) {
	err = c.HttpCli().JsonGet("/ipc/wingui/screen/findall?f="+f, &ret)
	return ret, err
}

func (c *Cli) ScreenClick(f string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/screen/click?f=%s", f), nil)
	return err
}

func (c *Cli) ScreenClickCenter(f string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/screen/clickcenter?f=%s", f), nil)
	return err
}

func (c *Cli) ScreenFind(f string) (ret []interface{}, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/screen/find?f=%s", f), &ret)
	return
}

func (c *Cli) ScreenWait(f string, m int) ([]byte, error) {
	url := fmt.Sprintf("/ipc/wingui/screen/wait?f=%s&m=%d", f, m)

	ret, err := c.HttpCli().RawGet(url)
	if err != nil {
		fmt.Printf(err.Error())
	}
	fmt.Println(ret)
	return ret, err
}

func (c *Cli) ScreenWaitClick(f string, m int) ([]byte, error) {
	url := fmt.Sprintf("/ipc/wingui/screen/waitclick?f=%s&m=%d", f, m)

	ret, err := c.HttpCli().RawGet(url)
	if err != nil {
		fmt.Printf(err.Error())
	}
	fmt.Println(ret)
	return ret, err
}

func (c *Cli) ScreenWaitClickCenter(f string, m int) ([]byte, error) {
	url := fmt.Sprintf("/ipc/wingui/screen/waitclickcenter?f=%s&m=%d", f, m)

	ret, err := c.HttpCli().RawGet(url)
	if err != nil {
		fmt.Printf(err.Error())
	}
	fmt.Println(ret)
	return ret, err
}

func (c *Cli) ScreenRes() ([]byte, error) {
	url := "/ipc/wingui/screen/res"

	ret, err := c.HttpCli().RawGet(url)
	if err != nil {
		fmt.Printf(err.Error())
	}
	return ret, err
}

func (c *Cli) ScreenSetRes(x, y, cor int) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/screen/setres?x=%d&y=%d&c=%d", x, y, cor), nil)
	return err
}

func (c *Cli) ScreenScale() (ret []int, err error) {
	err = c.HttpCli().JsonGet("/ipc/wingui/screen/scale", &ret)
	return ret, err
}

func (c *Cli) ScreenSize() (ret []int, err error) {
	err = c.HttpCli().JsonGet("/ipc/wingui/screen/size", &ret)
	return ret, err
}

func (c *Cli) ScreenShot(frmt string) (ret []byte, err error) {
	url := fmt.Sprintf("/ipc/wingui/screen/shot?fmt=%s", frmt)

	ret, err = c.HttpCli().RawGet(url)
	if err != nil {
		fmt.Print(err.Error())
	}
	return ret, err
}

func (c *Cli) ScreenDsInternal() error {
	err := c.HttpCli().JsonGet("/ipc/wingui/screen/dsinternal", nil)
	return err
}

func (c *Cli) ScreenDsExternal() error {
	err := c.HttpCli().JsonGet("/ipc/wingui/screen/dsexternal", nil)
	return err
}

func (c *Cli) ScreenDsClone() error {
	err := c.HttpCli().JsonGet("/ipc/wingui/screen/dsclone", nil)
	return err
}

func (c *Cli) ScreenDsExtend() error {
	err := c.HttpCli().JsonGet("/ipc/wingui/screen/dsextend", nil)
	return err
}

/***************** Dialog Functions *****************/

func (c *Cli) DialogColor(t, d string) (ret map[string]int, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/color?t=%s&d=%s", t, d), &ret)
	return ret, err
}

func (c *Cli) DialogDate(t, m, d, f string) (ret string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/date?t=%s&m=%s&d=%s&f=%s", t, m, d, f), &ret)
	return ret, err
}

func (c *Cli) DialogEntry(t, m, d string) (ret string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/entry?t=%s&m=%s&d=%s", t, m, d), &ret)
	return ret, err
}

func (c *Cli) DialogError(t, m string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/error?t=%s&m=%s", t, m), nil)
	return err
}

func (c *Cli) DialogFile(t, f, d string) (ret string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/file?t=%s&f=%s&d=%s", t, f, d), &ret)
	return ret, err
}

func (c *Cli) DialogFileMulti(t, f string) (ret []string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/filemulti?t=%s&f=%s", t, f), &ret)
	return ret, err
}

func (c *Cli) DialogInfo(t, m string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/info?t=%s&m=%s", t, m), nil)
	return err
}

func (c *Cli) DialogList(t, m, f string) (ret string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/list?t=%s&m=%s&f=%s", t, m, f), &ret)
	return ret, err
}

func (c *Cli) DialogListMulti(t, m, f string) (ret []string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/listmulti?t=%s&m=%s&f=%s", t, m, f), &ret)
	return ret, err
}

func (c *Cli) DialogPassword(t, m string) (ret string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/password?t=%s&m=%s", t, m), &ret)
	return ret, err
}

func (c *Cli) DialogQuestion(t, m string) (ret bool, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/question?t=%s&m=%s", t, m), &ret)
	return ret, err
}

func (c *Cli) DialogWarn(t, m string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/dialog/warn?t=%s&m=%s", t, m), nil)
	return err
}

/***************** Process Functions *****************/

func (c *Cli) ProcAll() (ret []map[string]interface{}, err error) {
	err = c.HttpCli().JsonGet("/ipc/wingui/proc/all", &ret)
	return ret, err
}

func (c *Cli) ProcPids() (ret []int, err error) {
	err = c.HttpCli().JsonGet("/ipc/wingui/proc/pids", &ret)
	return ret, err
}

func (c *Cli) ProcKill(cmd int) (ret string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/proc/kill?cmd=%d", cmd), &ret)
	return ret, err
}

func (c *Cli) ProcExec(cmd string) error {
	err := c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/proc/exec?cmd=%s", cmd), nil)
	return err
}

func (c *Cli) ProcName(pid int) (ret string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/proc/name?pid=%d", pid), &ret)
	return ret, err
}

func (c *Cli) ProcPath(pid int) (ret string, err error) {
	err = c.HttpCli().JsonGet(fmt.Sprintf("/ipc/wingui/proc/path?pid=%d", pid), &ret)
	return ret, err
}

func NewCli() *Cli {
	ret := &Cli{ApiCli: api.NewApiCli()}
	return ret
}
