module go.digitalcircle.com.br/open/replaycli-go

go 1.17

//replace (
//	go.digitalcircle.com.br/open/httpcli => ../httpcli
//)
require (
	github.com/Microsoft/go-winio v0.5.2
	github.com/gorilla/websocket v1.4.2
	github.com/mitchellh/go-ps v1.0.0
	go.digitalcircle.com.br/open/httpcli v0.0.0-20211031093505-ecf33aed8afb
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9 // indirect
)
